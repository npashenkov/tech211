import pygame, sys
from pygame.locals import *

pygame.init()
DisplaySurface = pygame.display.set_mode((400, 300))
pygame.display.set_caption('Hello Font!')

whiteColor = (255, 255, 255)
greenColor = (0, 255, 0)
blueColor = (0, 0, 128)

fontObj = pygame.font.Font('arial.ttf', 32)
textSurfaceObj = fontObj.render('Hello Font!', True, greenColor, blueColor)
textRectObj = textSurfaceObj.get_rect()
textRectObj.center = (200, 150)

while True: # main game loop
  DisplaySurface.fill(whiteColor)
  DisplaySurface.blit(textSurfaceObj, textRectObj)

  for event in pygame.event.get():
    if event.type == QUIT:
      pygame.quit()
      sys.exit()
    pygame.display.update()
