import pygame, sys
from pygame.locals import *

pygame.init()
pygame.display.set_caption('Traffic Light')

W = 500 # window width
H = 500 # window height
ws = pygame.display.set_mode((W, H))  # window surface
#ds = pygame.Surface((W,H), pygame.SRCALPHA, 32)
ds = ws.convert_alpha() # display surface

FPS = 30 # frames per second setting
fpsClock = pygame.time.Clock()
frameNum = 1;   # variable to keep track of animation frames

while True: # main game loop

    ws.fill((0, 0, 0))

    pygame.draw.circle(ds, (0, 255, 0, 100), (int(W/2), int(H*3/4)), 50)
    pygame.draw.circle(ds, (255, 255, 0, 100), (int(W/2), int(H*1/2)), 50)
    pygame.draw.circle(ds, (255, 0, 0, 100), (int(W/2), int(H*1/4)), 50)

    if frameNum < 30:
        pygame.draw.circle(ds, (0, 255, 0), (int(W/2), int(H*3/4)), 50)
    elif frameNum < 60:
        pygame.draw.circle(ds, (255, 255,0), (int(W/2), int(H*1/2)), 50)
    else:
        pygame.draw.circle(ds, (255, 0, 0), (int(W/2), int(H*1/4)), 50)

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    if frameNum < 90:
        frameNum += 1
    else:
        frameNum = 0

    ws.blit(ds, (0,0))
    pygame.display.update()
    fpsClock.tick(FPS)
