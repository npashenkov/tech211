import pygame, sys
from pygame.locals import *

pygame.init()
pygame.display.set_caption('Traffic Light')

W = 500 # window width
H = 500 # window height
ws = pygame.display.set_mode((W, H))  # window surface
#ds = pygame.Surface((W,H), pygame.SRCALPHA, 32)
ds = ws.convert_alpha() # display surface

FPS = 30 # frames per second setting
fpsClock = pygame.time.Clock()
frameNum = 1;   # variable to keep track of animation frames

walkImg = pygame.image.load('trafficlight-walk.png')

class TrafficLight:

    def __init__(self):
        self.setState('green')
        self.redXY = (int(W/2), int(H*1/4))
        self.yellowXY = (int(W/2), int(H*1/2))
        self.greenXY = (int(W/2), int(H*3/4))
        self.blinking = False
        self.lastTick = pygame.time.get_ticks()
        self.walkPressed = False

    def setState(self, stateStr):
        self.state = stateStr
        self.lastTick = pygame.time.get_ticks()

    def setBlinking(self, blinkBool):
        self.blinking = blinkBool
        self.setState('red')

    def drawLight(self, rgbaColor, xyPos):
        pygame.draw.circle(ds, rgbaColor, xyPos, 50)

    def update(self):

        if self.state == 'green' or self.state == 'walk':
            if pygame.time.get_ticks() >= self.lastTick + 5000:
                self.walkPressed = False;
                self.setState('yellow')

        elif self.state == 'yellow':
            if pygame.time.get_ticks() >= self.lastTick + 1000:
                self.setState('red')

        elif self.state == 'red':
            if self.blinking == True:
                if pygame.time.get_ticks() >= self.lastTick + 500:
                    self.setState('off')
            elif pygame.time.get_ticks() >= self.lastTick + 5000:
                if self.walkPressed == True:
                    self.setState('walk')
                else:
                    self.setState('green')

        elif self.state == 'off':
            if pygame.time.get_ticks() >= self.lastTick + 500:
                self.setState('red')

    def draw(self):

        if self.state == 'green' or self.state == 'walk':
            self.drawLight((0, 255, 0, 255), self.greenXY)
        else:
            self.drawLight((0, 255, 0, 100), self.greenXY)

        if self.state == 'yellow':
            self.drawLight((255, 255, 0, 255), self.yellowXY)
        else:
            self.drawLight((255, 255, 0, 100), self.yellowXY)

        if self.state == 'red':
            self.drawLight((255, 0, 0, 255), self.redXY)
        else:
            self.drawLight((255, 0, 0, 100), self.redXY)

        if self.state == 'walk':
            w,h = walkImg.get_size()
            walkImgScaled = pygame.transform.scale(walkImg, (int(w*0.20), int(h*0.20)))
            x,y = self.greenXY
            x -= 30
            y -= 40
            ds.blit(walkImgScaled, (x,y))

trafficLight = TrafficLight()

while True: # main game loop

    ws.fill((0, 0, 0))
    #ws.fill((255, 255, 255))
    trafficLight.update()
    trafficLight.draw()

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        elif event.type == pygame.MOUSEBUTTONDOWN:
            trafficLight.setBlinking(True)
            print("mouse down!")
        elif event.type == pygame.MOUSEBUTTONUP:
            trafficLight.setBlinking(False)
            print("mouse up!")
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                print("pressed walk button..")
                trafficLight.walkPressed = True

    """if frameNum < 90:
        frameNum += 1
    else:
        frameNum = 0"""

    ws.blit(ds, (0,0))

    pygame.display.update()
    fpsClock.tick(FPS)
