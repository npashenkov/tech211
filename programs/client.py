import socket
import sys

try:
    # create a TCP socket
    s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
except socket.error as msg:
    print("Error code: " + str(msg[0]) + ", Error message: " + msg[1])
    sys.exit()

print("created socket")

#host = 'www.google.com'
#port = 80
host = socket.gethostname()
port = 8888

try:
    remote_ip = socket.gethostbyname( host )
except socket.gaierror:
    print("hostname could not be rsolved")
    sys.exit()

print("ip address of " + host + " is " + remote_ip)

# connect to remote server
s.connect((remote_ip, port))

print("socket connected to " + host + " on ip " + remote_ip)
"""
message = "GET / HTTP/1.1\r\n\r\n"

try:
    s.sendall(message.encode())
except socket.error:
    print("failed to send")
    sys.exit()

print ("sent message successfully")
"""
reply = s.recv(1024)
print( reply.decode() )

s.close()
