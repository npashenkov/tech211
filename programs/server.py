import socket
import sys
import time

host = ''    # all available interfaces
port = 8888  # arbitrary port #

s = socket.socket( socket.AF_INET, socket.SOCK_STREAM )
print("created socket")

try:
    s.bind((host, port))
except socket.error as msg:
    print("bind failed.  Error Code: " + str(msg[0]) + " Message: " + msg[1])
    sys.exit()

print("socket bind complete")

s.listen(10)
print("socket is listening")

conn, addr = s.accept()

print("connected with " + addr[0] + ": " + str(addr[1]))

print("send time to the socket")
currentTime = time.ctime( time.time() ) + "\r\n"
conn.send( currentTime.encode('ascii'))

conn.close()
s.close()














#########
