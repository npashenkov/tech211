import pygame
from transitions import Machine
from random import randint, uniform

vec = pygame.math.Vector2

WIDTH = 800
HEIGHT = 600
FPS = 60
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
DARKGRAY = (40, 40, 40)

# Mob properties
MOB_SIZE = 32
MAX_SPEED = 5
MAX_FORCE = 0.1
APPROACH_RADIUS = 120

WANDER_RING_DISTANCE = 150
WANDER_RING_RADIUS = 50
RAND_TARGET_TIME = 500

class Mob(pygame.sprite.Sprite):

    states = ['paused', 'seeking', 'wandering']


    def __init__(self):
        self.groups = all_sprites
        pygame.sprite.Sprite.__init__(self, self.groups)
        self.image = pygame.Surface((MOB_SIZE, MOB_SIZE))
        self.image.fill(YELLOW)
        self.rect = self.image.get_rect()
        self.pos = vec(randint(0, WIDTH), randint(0, HEIGHT))
        self.vel = vec(MAX_SPEED, 0).rotate(uniform(0, 360))
        self.acc = vec(0, 0)
        self.rect.center = self.pos
        self.last_update = 0

        self.machine = Machine(model=self, states=Mob.states, initial='seeking')
        self.machine.add_transition(trigger='pause', source='*', dest='paused')
        self.machine.add_transition(trigger='go_seek', source='*', dest='seeking')
        self.machine.add_transition(trigger='go_wander', source='*', dest='wandering')

    def follow_mouse(self):
        mpos = pygame.mouse.get_pos()
        self.acc = (mpos - self.pos).normalize() * 0.5

    def seek(self, target):
        self.desired = (target - self.pos).normalize() * MAX_SPEED
        steer = (self.desired - self.vel)
        if steer.length() > MAX_FORCE:
            steer.scale_to_length(MAX_FORCE)
        return steer

    def seek_with_approach(self, target):
        self.desired = (target - self.pos)
        dist = self.desired.length()
        self.desired.normalize_ip()
        if dist < APPROACH_RADIUS:
            self.desired *= dist / APPROACH_RADIUS * MAX_SPEED
        else:
            self.desired *= MAX_SPEED
        steer = (self.desired - self.vel)
        if steer.length() > MAX_FORCE:
            steer.scale_to_length(MAX_FORCE)
        return steer

    def wander(self):
        # select random target every few sec
        now = pygame.time.get_ticks()
        if now - self.last_update > RAND_TARGET_TIME:
            self.last_update = now
            self.target = vec(randint(0, WIDTH), randint(0, HEIGHT))
        return self.seek(self.target)


    def update(self):

        if(self.state == 'seeking'):
            self.acc = self.seek_with_approach(pygame.mouse.get_pos())
        elif(self.state == 'wandering'):
            self.acc = self.wander()

        # equations of motion
        self.vel += self.acc

        if self.vel.length() > MAX_SPEED:
            self.vel.scale_to_length(MAX_SPEED)
        self.pos += self.vel
        if self.pos.x > WIDTH:
            self.pos.x = 0
        if self.pos.x < 0:
            self.pos.x = WIDTH
        if self.pos.y > HEIGHT:
            self.pos.y = 0
        if self.pos.y < 0:
            self.pos.y = HEIGHT
        self.rect.center = self.pos

    def draw_vectors(self):
        scale = 25
        # vel
        pygame.draw.line(screen, GREEN, self.pos, (self.pos + self.vel * scale), 5)
        # desired
        pygame.draw.line(screen, RED, self.pos, (self.pos + self.desired * scale), 5)
        # approach radius
        pygame.draw.circle(screen, WHITE, pygame.mouse.get_pos(), APPROACH_RADIUS, 1)

pygame.init()
screen = pygame.display.set_mode((WIDTH, HEIGHT))
clock = pygame.time.Clock()

all_sprites = pygame.sprite.Group()
mob = Mob()
paused = False
show_vectors = False
running = True

while running:
    clock.tick(FPS)
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                running = False
            if event.key == pygame.K_m:
                Mob()
            if event.key == pygame.K_SPACE:
                for sprite in all_sprites:
                    sprite.pause()
                print(mob.state)
            if event.key == pygame.K_v:
                show_vectors = not show_vectors
            if event.key == pygame.K_s:
                for sprite in all_sprites:
                    sprite.go_seek()
                print(mob.state)
            if event.key == pygame.K_w:
                for sprite in all_sprites:
                    sprite.go_wander()
                print(mob.state)

    #if not paused:
    if(mob.state != 'paused'):
        all_sprites.update()

    pygame.display.set_caption("{:.2f}".format(clock.get_fps()))
    screen.fill(DARKGRAY)
    all_sprites.draw(screen)
    if show_vectors:
        for sprite in all_sprites:
            sprite.draw_vectors()
    pygame.display.flip()
