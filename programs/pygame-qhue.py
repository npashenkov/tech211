import pygame, sys
from pygame.locals import *
from qhue import Bridge

pygame.init()
pygame.display.set_caption('Qhue Test')

ds = pygame.display.set_mode((400, 300))

hue_user = "nUS6rndMcOE0XIWIVsxMA8EA-0xJH4Yqk1A-hCDf"
#hue_url = "192.168.2.100"

#hue_user = "hM8rQaOtrJo-FEHKiOCW2pcLQpVJwHrTFOPh67Ci"
hue_url = "192.168.86.21"

b = Bridge(hue_url, hue_user)
lights = b.lights

#print(lights[17]()['state']['hue'])
print(lights[1]().get('state').get('hue'))

#set up colors
whiteColor = (255, 255, 255)
greenColor = (0, 255, 0)
redColor = (255, 0, 0)
blueColor = (0, 0, 255)
ds.fill(whiteColor)


while True: # main game loop
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_g:
                print("set green hue..")
                lights[1].state(hue=20000)
            if event.key == pygame.K_r:
                print("set red hue..")
                lights[1].state(hue=0)
            if event.key == pygame.K_1:
                print("turn light 1 on..")
                lights[1].state(on=True)
            if event.key == pygame.K_0:
                print("turn light 0 off..")
                lights[1].state(on=False)

    pygame.display.update()
