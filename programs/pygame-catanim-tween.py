import pygame, sys
from pygame.locals import *
import pytweening as tween

pygame.init()

FPS = 30 # frames per second setting
fpsClock = pygame.time.Clock()

# set up the window
DisplaySurface = pygame.display.set_mode((400, 300), 0, 32)
pygame.display.set_caption('Animation')

whiteColor = (255, 255, 255)
catImg = pygame.image.load('cat.png')
catx = 10
caty = 100
tweenx = catx
direction = 'right'
frameNum = 0

while True: # the main game loop
    DisplaySurface.fill(whiteColor)

    if direction == 'right':
        catx += 5

        if catx >= 270:
            direction = 'left'
        else:
            tweenx = tween.easeInOutSine(float(catx) / 280.0) * 280

        print(tweenx)

    elif direction == 'left':
        catx -= 5

        if catx <= 10:
            direction = 'right'
        else:
            tweenx = tween.easeInOutElastic(float(catx) / 280.0) * 280

        print(tweenx)

    DisplaySurface.blit(catImg, (tweenx, caty))

    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()

    pygame.display.update()
    fpsClock.tick(FPS)
