import pygame, sys
from pygame.locals import *

pygame.init()
pygame.display.set_caption('Hello Pygame Primitives!')

ds = pygame.display.set_mode((400, 300))

#set up colors
whiteColor = (255, 255, 255)
greenColor = (0, 255, 0)
redColor = (255, 0, 0)
blueColor = (0, 0, 255)
ds.fill(whiteColor)

pygame.draw.line(ds, greenColor, (0,0),(400,300))
pygame.draw.circle(ds, redColor, (200, 150), 50)
pygame.draw.rect(ds, blueColor, (10, 10, 100, 50))
pygame.draw.polygon(ds, greenColor, ((50, 50), (150, 70), (200, 200), (70, 180)))

while True: # main game loop
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.update()
